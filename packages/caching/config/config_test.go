package config_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/VagueCoder0to.n/Clients-Collection/packages/caching/config"
)

func TestConfig(t *testing.T) {
	assert.NotEmpty(t, config.RedisAddr, "env REDIS_ADDRESS shouldn't be empty")
	// assert.NotEmpty(t, config.RedisPassword, "env REDIS_PASSWORD shouldn't be empty")
}
