package config

import "os"

var (
	RedisAddr     string
	RedisPassword string
)

func init() {
	RedisAddr = os.Getenv("REDIS_ADDRESS")
	RedisPassword = os.Getenv("REDIS_PASSWORD")
}
