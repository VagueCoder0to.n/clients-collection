package caching

import (
	"context"
	"fmt"
	"regexp"

	redis "github.com/go-redis/redis/v8"
	"gitlab.com/VagueCoder0to.n/Clients-Collection/packages/caching/config"
)

// RedisClient is a custom struct for holding both redis-client and also the context.
type RedisClient struct {
	Client *redis.Client
	Ctx    context.Context
}

// NewRedisClient initiates new db client object and returns.
func NewRedisClient() (*RedisClient, error) {

	// Check for empty db and access
	var db int
	db, err = DatabaseNumber()
	if err != nil {
		return nil, err
	}

	// Basic context type
	ctx := context.Background()

	// Creating new redis-client
	client := redis.NewClient(&redis.Options{
		Addr:     config.RedisAddr,
		Password: config.RedisPassword,
		DB:       db,
	})

	// creating new *RedisClient object with client & context included
	return &RedisClient{
		Client: client,
		Ctx:    ctx,
	}, nil
}

// DatabaseNumber function checks for unused dbs in redis-server and returns the index.
func DatabaseNumber() (int, error) {
	// Temporary redis client to check the keyspace info.
	// Note: Creating client isn't considered as db in use. Adding keys does.
	rc := redis.NewClient(&redis.Options{
		Addr:     config.RedisAddr,
		Password: config.RedisPassword,
		DB:       0,
	})

	// Getting the keyspace info to check if dbs are in use.
	info := rc.Info(context.Background(), "keyspace").Val()

	// Match to db number pattern similar to
	pattern := regexp.MustCompile(`(db\d[0-5]):|(db\d):`)
	dbs := pattern.FindAllString(info, -1)

	total := len(dbs)
	if total == 16 {
		return -1, fmt.Errorf("overflow Error: All 16 DBs of Redis are full")
	}

	return total, nil
}

// Ping pings the redis-server
func Ping() (string, error) {
	// Temporary redis client to check the keyspace info.
	// Note: Creating client isn't considered as db in use. Adding keys does.
	rc := redis.NewClient(&redis.Options{
		Addr:     config.RedisAddr,
		Password: config.RedisPassword,
		DB:       0,
	})
	pong, err = rc.Ping(context.Background()).Result()
	if err != nil {
		return "", err
	}
	return pong, nil
}
