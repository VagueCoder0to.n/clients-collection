# Caching (Package in Clients-Collection)

## What is in Clients-Collection?
A collection of different agent (Redis, RabbitMQ, MongoDB, PostgreSQL, etc.) 's clients code packaged for reusability. This only includes the simple and repetitive functions that can be reused in projects that implement.

## What is Caching package?
As the name suggests, the package Caching helps in any purpose caching. It operates on Redis agent, with the help of module [github.com/go-redis/redis/v8](github.com/go-redis/redis/v8). Frequently used methods like Set, Get, Flush, Count Keys, etc., are defined for usage.

## Index
```go
func DatabaseNumber() (int, error)
func Ping() (string, error)
type RedisClient
    func NewRedisClient() (*RedisClient, error)
    func (r *RedisClient) CountKeys() (int, error)
    func (r *RedisClient) DeleteKey(key string) bool
    func (r *RedisClient) FlushAll() bool
    func (r *RedisClient) FlushDB() bool
    func (r *RedisClient) Get(key string) (string, error)
    func (r *RedisClient) KeysStartingWith(start string) ([]string, error)
    func (r *RedisClient) Set(key, val string) error
```

## Functions
### func [DatabaseNumber](caching.go)
```go
func DatabaseNumber() (int, error)
```
Function returns the next usable number of Redis db in range [0, 16). If all 16 dbs are full, it will return error.

### func [Ping](caching.go)
```go
func Ping() (string, error)
```
Ping returns pong on successful connection to redis-server, else error.

## Types
#### type [RedisClient](caching.go)
```go
type RedisClient struct {
	Client *redis.Client
	Ctx    context.Context
}
```
Type RedisClient contains [github.com/go-redis/redis/v8](github.com/go-redis/redis/v8) package's Client type, and a Context, initially defined as `context.Background()`. Ctx is exported for usability with child context types.

#### func [NewRedisClient](caching.go)
```go
func NewRedisClient() (*RedisClient, error)
```
Creates a RedisClient object with [github.com/go-redis/redis/v8](github.com/go-redis/redis/v8) package's Client type as Client, and `context.Background()` as Ctx. Returns error if initialization fails due to server down, db full, etc.

#### func (*RedisClient) [CountKeys](operations.go)
```go
func (r *RedisClient) CountKeys() (int, error)
```
Counts the number of keys in current db. Returns error if server down, etc.

#### func (*RedisClient) [DeleteKey](operations.go)
```go
func (r *RedisClient) DeleteKey(key string) bool
```
Deletes the given key in current db. Returns true if operation successful, else false.

#### func (*RedisClient) [FlushAll](operations.go)
```go
func (r *RedisClient) FlushAll() bool
```
Flushes all the keys in all the dbs. Returns true if operation successful, else false.

#### func (*RedisClient) [FlushDB](operations.go)
```go
func (r *RedisClient) FlushDB() bool
```
Flushes all keys in current db. Returns true if operation successful, else false.

#### func (*RedisClient) [Get](operations.go)
```go
func (r *RedisClient) Get(key string) (string, error)
```
Gets the value for the given key in current db. Returns error if key not found. Also, returns error if server down, etc.

#### func (*RedisClient) [KeysStartingWith](operations.go)
```go
func (r *RedisClient) KeysStartingWith(start string) ([]string, error)
```
Returns a slice of keys that start with the given pattern (substring) in current db. Returns error if no keys are found. Also, returns error if server down, etc.

#### func (*RedisClient) [Set](operations.go)
```go
func (r *RedisClient) Set(key, val string) error
```
Sets a key to value in current db. In other words, it inserts key-value pair. If key already exists, Set method will update the corresponding value. Returns error if inserting operation fails due to server issue, etc.

---

## Message to Viewer
A geek to geek: This is an open-source, in-development, no-rush kinda project. However, if you somehow liked the idea and wish to contribute; or liked the code snippet and wish to use for your own; I'd be glad to share.

You can let me know your ideas, or any issues and queries with this project, or if you want to further discuss. Just write to me at (Click): [VagueCoder0to.n@gmail.com](mailto:VagueCoder0to.n@gmail.com?subject=%5BGITLAB%3A%20Clients-Collection%27s%20Caching%5D%20Your%20Subject%20Here&body=Hello%20Vague%2C%0A%0A)

---

## Return to [Clients-Collection](gitlab.com/VagueCoder0to.n/Clients-Collection)