# :warning: `This is Under Continuous-Integration!!`

## What is in Clients-Collection?
A collection of different agent (Redis, RabbitMQ, MongoDB, PostgreSQL, etc.) 's clients code packaged for reusability. This only includes the simple and repetitive functions that can be reused in projects that implement.

## Packages / Clients
The individual package directory has respective README for walkthrough. The Index is as follows:

Sno. | Package (of client) | Path | Short Intro
----:|:-------------------:|:----:|:-----------
1 | [Caching](packages/caching/README.md) | [packages/caching](packages/caching) | The package Caching helps in any purpose caching, and operates on Redis agent.
---

## Message to Viewer
A geek to geek: This is an open-source, in-development, no-rush kinda project. However, if you somehow liked the idea and wish to contribute; or liked the code snippet and wish to use for your own; I'd be glad to share.

You can let me know your ideas, or any issues and queries with this project, or if you want to further discuss. Just write to me at (Click): [VagueCoder0to.n@gmail.com](mailto:VagueCoder0to.n@gmail.com?subject=%5BGITLAB%3A%20Clients-Collection%5D%20Your%20Subject%20Here&body=Hello%20Vague%2C%0A%0A)

---
## Happy Coding !! :metal:
