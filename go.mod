module gitlab.com/VagueCoder0to.n/Clients-Collection

go 1.15

require (
	github.com/go-redis/redis/v8 v8.10.0
	github.com/stretchr/testify v1.7.0
)
